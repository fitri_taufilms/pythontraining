import random

#compute random lists
randa = []
randb = []


for a in range(0, 10):
    a = random.randint(0, 10)
    randa.append(a)
for b in range(0, 10):
    b = random.randint(0, 10)
    randb.append(b)


print("\nSet A: " + str(randa))
print("Set B: " + str(randb))


#remove duplicates on the random lists
rand_a = set(randa)
rand_b = set(randb)


#compare the two random lists
match = []
for check1 in rand_a:
    if check1 in rand_b:
        match.append(check1)


print ("Common number between the 2 lists: "
       + str(match))